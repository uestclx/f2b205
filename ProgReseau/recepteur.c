#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include </usr/include/linux/in.h>
#include </usr/include/linux/in6.h>
#include <arpa/inet.h>

#define BUFSIZE 100000

int main(int argc, char **argv) {
  int sfd, s, rsz, r;
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  ssize_t nrecv;
  char buf[BUFSIZE];
  struct sockaddr_storage from;
  socklen_t fromlen;
  char host[NI_MAXHOST], service[NI_MAXSERV];
  struct ip_mreqn bcast_arg;
  struct ipv6_mreq bcast6_arg;
  struct in_addr inp;

  if (argc != 3) {
    printf("Usage: %s port\n", argv[0]);
    exit(EXIT_FAILURE);
  }

 /* Construction de l'adresse locale (pour bind) */
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;           /* Force IPv4 */
  hints.ai_socktype = SOCK_DGRAM;       /* Datagram socket */
  hints.ai_flags = AI_PASSIVE;          /* Pour l'adresse IP joker */
  hints.ai_flags |= AI_V4MAPPED|AI_ALL; /* IPv4 remappe en IPv6 */
  hints.ai_protocol = 0;                /* Any protocol */
//  hints.ai_protocol = PF_INET6;                /* protocol IPv6 */

  s = getaddrinfo(argv[1], argv[2], &hints, &result);
  if (s != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
    exit(EXIT_FAILURE);
  }

  /* getaddrinfo() retourne une liste de structures d'adresses.
     On essaie chaque adresse jusqu'a ce que bind(2) reussisse.
     Si socket(2) (ou bind(2)) echoue, on (ferme la socket et on)
     essaie l'adresse suivante. cf man getaddrinfo(3) */
  for (rp = result; rp != NULL; rp = rp->ai_next) {

    /* Creation de la socket */
    sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (sfd == -1)
      continue;

    /* Association d'un port a la socket */
    r = bind(sfd, rp->ai_addr, rp->ai_addrlen);
    if (r == 0)
      break;            /* Succes */
    close(sfd);
  }

  if (rp == NULL) {     /* Aucune adresse valide */
    perror("bind");
    exit(EXIT_FAILURE);
  }

  printf("getaddrinfo addr = %s\n", rp->ai_addr->sa_data);
#if 0  
  if (inet_aton(rp->ai_addr->sa_data, &bcast_arg.imr_multiaddr) <= 0) {
    printf("address is not valide\n");
    exit(EXIT_FAILURE);
  }
#endif
  bcast_arg.imr_multiaddr.s_addr = inet_addr(rp->ai_addr->sa_data);
  bcast_arg.imr_address.s_addr = htonl(INADDR_ANY);
  bcast_arg.imr_ifindex = 0;

switch (rp->ai_family) {
  case AF_INET:
    memcpy(&mreqn.imr_multiaddr.s_addr, &(((struct sockaddr_in*)(rp->ai_addr))->sin_addr), sizeof(struct in_addr));
    mreqn.imr_address.s_addr = INADDR_ANY;
    merqn.imr_ifindex = 0;
  case AF_INET6

}









  if (setsockopt(sfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &bcast_arg, sizeof(bcast_arg)) == 0) {
    printf("broadcast group is added.\n");
  } else {
    perror("join broadcast group");
    exit(EXIT_FAILURE);
  }
  
  freeaddrinfo(result); /* Plus besoin */

  /* Force la taille du buffer de reception de la socket */
  rsz = 80000;
  if ( setsockopt(sfd, SOL_SOCKET, SO_RCVBUF, &rsz, sizeof(rsz)) == 0 )
    printf("SO_RCVBUF apres forcage: %d octets\n", rsz);
  else
    perror("setsockopt SO_RCVBUF");



  /* Boucle de communication */
  for (;;) {
    /* Reception donnees */
    fromlen = sizeof(from);
    nrecv = recvfrom(sfd, buf, sizeof(buf), 0, (struct sockaddr *)&from, &fromlen);
    if (nrecv == -1) {
      perror("Erreur en lecture socket\n");
      exit(EXIT_FAILURE);
    }
    printf("Recu %zd octets\n", nrecv);

    /* Reconnaissance de la machine cliente */
    s = getnameinfo((struct sockaddr *)&from, fromlen,
			host, NI_MAXHOST, service, NI_MAXSERV,
			NI_NUMERICHOST | NI_NUMERICSERV);
    if (s == 0)
      printf("Emetteur '%s'  Port '%s'\n", host, service);
    else
      printf("Erreur: %s\n", gai_strerror(s));
  }
}
