/* 
 * Auteur(s):
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define TABSIZE 512

int main(int argc, char **argv) {
  int pid, longueur;
  char tab[TABSIZE], *s;
/*
  char path[TABSIZE];
  char command[TABSIZE];
  char *index, *p, *pin, *ppath;
*/
  for (;;) {
    fputs("petit_shell...> ", stdout);	/* Affichage d'un prompt */
    s = fgets(tab, TABSIZE, stdin);

    if (s == NULL) {
      fprintf(stderr, "Fin du Shell\n");
      exit(EXIT_SUCCESS);
    }

    longueur = strlen(s);
    tab[longueur - 1] = '\0';
/*
    p = strrchr(s, '/');
    printf("lastchar = %c", *p);
    ppath = path;
    pin = s;
    while(pin <= p){
      *ppath = *pin;
      ppath++;
      pin++;
    }
*/

    pid = fork();
    if (pid == 0) {
      execlp(tab, tab, (char *)NULL);
      printf("execlp doesn't work well\n");
      /* If no exit(0), when there is an error, the child processus will left in system */
      exit(0);
    } else {
      wait(NULL);
    }

    /* Actions:
     * 
     * Si dans pere alors
     *   wait(NULL);
     * sinon alors
     *   execution de la commande recuperee dans tab;
     *   message d'erreur: fprintf(stderr, "Erreur dans le exec\n")
     * fin si
     */

  }
}
