#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

void sigcld_handler(int n) {
	wait(NULL);
}

int main(int argc, char **argv)
{
	int pid;
	int ret;
	char buf;
	int pipefd[2];

	signal(SIGCLD, sigcld_handler);
	if (pipe(pipefd) ==  -1) {
		perror("create pipe");
		exit(EXIT_FAILURE);
	}

	pid = fork();
	if (pid != 0) {
		/* father processus */
		close(pipefd[0]);
		ret = dup2(pipefd[1], 1);

		if (ret != 1) {
			printf("father process's stdout is not configured"
				" as pipe's input!\n");
			exit(EXIT_FAILURE);
		}
	} else {
		close(pipefd[1]);
		ret = dup2(pipefd[0], 0);
		if (ret != 0) {
			printf("child's stdin is not is not configured"
				" as pipe's ouput!\n");
			exit(EXIT_FAILURE);
		}
	}

	while(read(0, &buf, 1) > 0) {
		write(1, &buf, 1);
	}
	if (pid != 0) {
		close(pipefd[1]);
	} else {
		close(pipefd[0]);
	}
}


