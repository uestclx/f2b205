/*
 * Auteur(s):
 *
 * Cet programme refait ce que fait la commande "ls". Il donne des
 * informnations sur les caracteristiques de fichiers dont le nom est passe
 * en parametre.
 *
 * Utilisation de la primitive stat(2) et de la fonction getpwuid(3).
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>

/* Petite fonction qui se charge d'envoyer les messages d'erreur
   et qui ensuite "suicide" le processus. */

void erreur_grave(char *msg) {
  perror(msg);
  exit(EXIT_FAILURE);
}

/* Fonction principale (fournie avec erreur(s?)) */

int main(int argc, char **argv) {
  struct stat status;
  int r;
  struct passwd *user_info;
  struct passwd *false_user;

  r = stat(argv[1], &status);

  /* 1_3 getpwuid always return the same address */
  user_info = getpwuid(status.st_uid);
  false_user = getpwuid(1000);
  printf("user_info_addr = %lld, false_info_addr = %lld\n", (long long int)user_info, (long long int)false_user);
  
  if (r < 0)
    erreur_grave("Stat");

  printf("Fichier %s:  mode: %X  Taille: %ld  Proprietaire: NO. = %d, Name = %s, false_uid(1000) = %s\n",
	argv[1], status.st_mode, status.st_size, status.st_uid, user_info->pw_name, false_user->pw_name);

  exit(EXIT_SUCCESS);
}
