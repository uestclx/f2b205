/* 2_1 show environment variable */

#include <stdio.h>
#include <stdlib.h>

extern char **environ;

int main(int argc, char **argv)
{
  char **index = environ;
  char *env_virable = NULL;

  /* 2_1 show system environment variable */
  while (*index != NULL) {
    printf("%s\n", *index);
    index++;
  }

  /* 2_2 show a specific environment variable */
  env_virable = getenv(argv[1]);
  printf("env_virable[%s] = %s\n", argv[1], env_virable);

  return 0;
}
